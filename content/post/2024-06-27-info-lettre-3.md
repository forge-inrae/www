---
title: Troisème info-lettre
subtitle: "Info-lettre de la forge INRAE #3"
date: 2024-06-27T17:00:00+02:00 # à décommenter pour afficher le post si la date est passée
tags: ["info-lettre"]
---

- [Tests de migration à venir sur la pré-production](#tests-de-migration-à-venir-sur-la-pré-production)
- [Comité des utilisateurs](#comité-des-utilisateurs)
- [Stratégie de migration](#stratégie-de-migration)
- [Focus sur les permissions par défaut](#focus-sur-les-permissions-par-défaut)
- [Café Numérique forge logicielle INRAE](#café-numérique-forge-logicielle-inrae)
- [Enquête côté INRAE sur les fonctionnalités GitLab premium/ultimate pour l'ESR](#enquête-côté-inrae-sur-les-fonctionnalités-gitlab-premiumultimate-pour-lesr)
- [Rappel planning prévisionnel](#rappel-planning-prévisionnel)
- [Autres questions](#autres-questions)

Chères et chers collègues,

Cette lettre d'information s'adresse à tous les utilisateurs de la future forge logicielle institutionnelle.

Le projet forge logicielle INRAE avance : nous sommes sur le point d'ouvrir une période de tests sur l'infrastructure de pré-production dans le courant de l'été.

La révision du calendrier prévisionnel des opérations nous permet de projeter le lancement de la forge INRAE à l'automne 2024. Les dates exactes seront précisées selon les résultats des différents tests réalisés d'ici septembre.

## Tests de migration à venir sur la pré-production

Courant août les données de la forgeMIA seront *dupliquées* vers un environnement de pré-production de forge INRAE. Cet environnement sera identique à celui de la forge institutionnelle finale, permettant une évaluation grandeur nature des outils développés par l'équipe opérationnelle depuis plusieurs mois. Ce test sera ainsi l'occasion :

- de s'assurer du bon fonctionnement des outils des sauvegarde, de restauration et de configuration d'un GitLab sur un environnement iso-prod ;
- d'identifier les verrous à lever pour préparer au mieux la bascule finale, en tant qu'administrateur comme en tant qu'utilisateur. Cela permettra de clarifier les impacts sur les utilisateurs, en particulier les adaptations nécessaires lors du changement d'instance.

ℹ️ Ayez à l'esprit que pour la bascule finale, l'URL de la forge institutionnelle sera différente de celle de forgeMIA du département MathNum, cette dernière instance devenant alors inopérante :

- forgeMIA actuelle : <https://forgemia.inra.fr>
- future forge INRAE : <https://forge.inrae.fr>

L'URL d'accès à la forge MIA devant changer, une redirection web assurera le bon fonctionnement des liens qui ont été disséminés sur les Internet depuis des années, notamment dans des publications qu'il n'est pas aisé de corriger.

À ce stade, un groupe d'utilisateurs beta-testeurs en cours de constitution participera à la validation fonctionnelle de la migration de leurs projets sur l'instance de pré-production évoquée ci-dessus. Si vous souhaitez tester l'état de vos projets une fois migrés, n'hésitez pas à vous faire connaître auprès de [Tovo Rabemanantsoa](mailto:tovo.rabemanantsoa@inrae.fr?subject=Participation%20tests%20forge%20logicielle%20INRAE) ou à vous inscrire sur cette issue du projet GitLab : <https://forgemia.inra.fr/forge-inrae/comut/test-pre-production/-/issues/1>.

## Comité des utilisateurs

Dans le cadre du projet forge logicielle institutionnelle, un comité des utilisateurs (ComUt) a été mis en place. Il regroupe à ce jour une trentaine de collègues de profils et horizons variés, permettant de maximiser les points de vue et les besoins pouvant être discutés.

Le ComUt se retrouve généralement toutes les deux semaines alternativement avec le comité de pilotage. Un ordre du jour transmis à l'avance, permet aux membres d'évaluer la pertinence de leur participation (selon que les sujets sont plutôt techniques ou organisationnels par exemple).

L'un des objectifs du ComUt est de procéder à l'identification et au pré-traitement des besoins fonctionnels sur le périmètre de la forge logicielle GitLab et sur l'outil de chat (Mattermost) qui y est adossé. Un processus de gestion de ces besoins a été établi et se décline comme suit :

[![thumbnail](/www/img/workflow_gestion_demandes_besoin.png 'Workflow gestion demandes et besoin utilisateurs')](/www/img/workflow_gestion_demandes_besoin.png)

Ainsi, le Comité d'utilisateurs suivra les demandes de nouvelles fonctionnalités, en mènera une analyse d'impact, en lien avec les membres de l'équipe opérationnelle le cas échéant. Il décidera alors s'il est nécessaire d'en remonter les conclusions au Comité de pilotage qui en dernier ressort étudiera la demande pour s'assurer de son alignement stratégique avec la politique Science Ouverte de l'Institut, l'écosystème en place et les forces de travail nécessaires. Une priorisation sera alors effectuée et la demande sera suivie en transparence sur un projet de la forge Inrae, ouvert à tous les usagers.

Pour simplifier la gestion des tickets, le projet GitLab sera partagé avec le support de niveau 1 réalisé par l'équipe opérationnelle. Les modalités pratiques permettant de distinguer une demande d'aide, des demandes de nouvelle fonctionnalité restent à définir, mais pourront s'appuyer sur les fonctionnalités propres à GitLab (ie. étiquette/label dédié, modèle de ticket).

Il est également possible de discuter de sujet relatif à la forge logicielle via le forum de la DipSO en utilisant le tag [#forge](https://forum.dipso.inrae.fr/tag/forge).

Si vous souhaitez rejoindre le ComUt, n'hésitez pas à vous faire connaître auprès de [Tovo Rabemanantsoa](mailto:tovo.rabemanantsoa@inrae.fr?subject=Participation%20comité%20utilisateurs%20forge%20logicielle%20INRAE).

## Stratégie de migration

Pour rappel, un scénario de migration des projets existants a été retenu. Il se déploie en trois phases temporelles distinctes selon la forge logicielle utilisée par les projets à migrer.  
Il existe dans INRAE des forges s'appuyant sur diverses solutions techniques telles que GitLab ou Redmine. Selon le type de forge, la migration et l'accompagnement se feront à différents moments :

1. Les projets sous ForgeMIA seront migrés à l'occasion d'une bascule de forgeMIA vers la nouvelle forge INRAE. De ce fait, l'ensemble des projets de forgeMIA constituera le contenu initial de la forge INRAE. Un travail préparatoire d'accompagnement des porteurs de projets est en cours, en lien avec des membres du comité d'utilisateurs pour faciliter la transition, les tests mentionnés ci-dessus en sont un élément ;
2. Les projets hébergés par d'autres forges GitLab (ie. forge ex-IRSTEA et autres forges de centre ou d'unité) pourront ensuite être migrés progressivement par les porteurs de projet qui bénéficieront d'un appui dédié : en premier lieu l'effort du support concernera les projets tournant sur des forges de type GitLab ;
3. Enfin les porteurs de projets hébergés sur des forges de type Redmine seront accompagnés en dernier ressort, en raison des incompatibilités techniques et organisationnelles entre les outils différents. Une étude au cas par cas devra être réalisée afin d'évaluer la pertinence de migrer et l'impact sur l'usage.

## Focus sur les permissions par défaut

Sur la nouvelle forge INRAE, par défaut seuls les utilisateurs disposant d'une adresse e-mail en inrae.fr seront autorisés à créer un nouveau groupe puis un nouveau projet. Cette contrainte permettra de mieux gérer la montée en charge et donc de maintenir une qualité de service satisfaisante.

De ce fait, les unités mixtes ayant plusieurs tutelles devront s'organiser pour qu'un agent INRAE initie une création de groupe puis délègue les accès à ses collègues.

De manière exceptionnelle, un compte externe peut être passé interne sur intervention d'un admin forge.

## Café Numérique forge logicielle INRAE

L'un des prochains Café Numérique sera consacré au projet forge logicielle INRAE. Il se tiendra sur le créneau du 26/08 à 16h. Réservez bien le créneau pour obtenir les dernières informations (une rediffusion sera disponible après coup).

## Enquête côté INRAE sur les fonctionnalités GitLab premium/ultimate pour l'ESR

Ça bouge du côté de l'ESR. Une enquête, purement informative, est en cours au niveau de l'ESR afin de recenser le nombre d'agents qui seraient intéressés par les [fonctionnalités avancées](https://about.gitlab.com/pricing/feature-comparison/) de GitLab dans les éditions premium et ultimate.

Pour aider à remonter les informations pour l'institut, vous êtes invités à remplir le bref sondage suivant (1 minute) : <https://sondages.inrae.fr/index.php/983564?lang=fr>.

## Rappel planning prévisionnel

- Juillet-Août : duplication des données de forgeMIA vers pré-prod forge INRAE
- Août-septembre : tests des projets forgeMIA migrés sur la pré-prod forge INRAE + tests préliminaires migration projets autres forges (ie. ex-IRSTEA)
- Septembre : 2e test de migration grandeur nature
- Courant automne : bascule de forgeMIA vers la forge INRAE, mise en place de redirections web pour assurer le maintien des liens depuis des sites externes
- Fin automne / hiver : migration progressive des projets des autres forges GitLab, notamment la forge ex-IRSTEA
- Ensuite, migration au fil de l'eau des projets hébergés sur d'autres outils, ie. forge Redmine

## Autres questions

N'hésitez pas à nous solliciter afin que nous mettions à jour la [FAQ](/www/page/faq/).

D'ici là, soyez assurés que nous œuvrons pour faire de ce projet un succès collectif !

Par les membres du comité de pilotage de la forge INRAE.
