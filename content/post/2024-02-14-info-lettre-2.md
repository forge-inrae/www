---
title: Deuxième info-lettre
subtitle: "Info-lettre de la forge INRAE #2"
date: 2024-02-13T19:00:00+02:00 # à décommenter pour afficher le post si la date est passée
tags: ["info-lettre"]
---

Chères et chers collègues,

Cette période de début d'année est propice pour partager avec vous les avancées et les perspectives du projet forge INRAE.

Depuis le mois d'avril dernier, le comité de pilotage a poursuivi ses travaux et a activement contribué à la consolidation de l'organisation en structurant une équipe opérationnelle et un comité des utilisateurs.

Un calendrier prévisionnel des opérations a été établi et nous permet de projeter le lancement de la forge INRAE sur 2024.

## Équipe opérationnelle

L'équipe opérationnelle est composée des agents du département scientifique MathNum en charge de l'exploitation de la forgeMIA[^1], ainsi que d'agents de la DipSO et de la DSI. Sa mission est de développer et de déployer la forge de portée institutionnelle, sur une infrastructure matérielle à la hauteur des attentes des agents et dans le respect des lignes directrices du Plan Données pour la Science d'INRAE[^2].

La DSI s'est également engagée en apportant son expertise au niveau de la sécurité des systèmes, elle fournit, supervise et gère les ressources numériques (machines virtuelles, réseau, espaces de stockages) nécessaires au fonctionnement du service dans la durée.

Enfin, une prestation de service a été contractualisée pour accompagner l'équipe opérationnelle dans le développement d'outils dédiés à l'exploitation de Gitlab et dans leur prise en main.

## Comité des utilisateurs

Le Comité des Utilisateurs représente les personnels de l'Institut. Il a pour vocation de s'assurer que les besoins en matière d'usages de la forge INRAE soient pris en compte de façon satisfaisante.

Une trentaine de personnes constitue le Comité d'Utilisateurs, il a pour mission de structurer la remontée des besoins de la communauté.

Il est également possible de partager des besoins complémentaires via le forum de la DipSO en utilisant le tag [#forge](https://forum.dipso.inrae.fr/tag/forge).

## Avancement des travaux

La solution pour l'hébergement de la forge INRAE a été validée et s'appuiera sur les ressources d'hébergement et de gestion de machines virtuelles (VM) de la DSI.

Les travaux de développement sont en cours pour permettre la mise en place d'un système d'information pour les services Gitlab. Ils tiennent compte des évolutions à venir en prévoyant une montée en charge progressive. Les outils en cours de développement sont conçus pour faciliter ces évolutions dans la durée.
Ces travaux s'inscrivent dans une démarche communautaire et devraient permettre à d'autres collectifs de s'en emparer, les faire évoluer et améliorer en retour.

L'ensemble du projet s'inscrit dans une démarche d'homologation de sécurité[^3] des systèmes d'information avec l'équipe RSSI d'INRAE.

## Lancement de la forge institutionnelle

Les travaux entrepris pour consolider le dispositif technique et organisationnel laissent entrevoir un passage en production de la forge institutionnelle en fin de premier semestre 2024, avec une date de repli à la rentrée de septembre le cas échéant.

La date précise sera communiquée au moins 2 mois en amont afin que les collectifs concernés puissent s'y préparer.

## Stratégie de migration

Un scénario de migration des projets existants a été retenu. Il se déploie en trois phases temporelles distinctes selon la forge logicielle utilisée. Pour rappel il existe dans INRAE des forges s'appuyant sur diverses solutions techniques telles que Gitlab ou Redmine. Selon le type de forge, la migration et l'accompagnement se feront à différents moments :

1. Les projets sous ForgeMIA seront migrés à l'occasion d'une bascule de forgeMIA vers la nouvelle forge INRAE. De ce fait, l'ensemble des projets de forgeMIA constituera le contenu initial de la forge INRAE. Un travail préparatoire d'accompagnement des porteurs de projets est en cours, en lien avec des membres du comité d'utilisateurs pour faciliter la transition ;
2. Les projets hébergés par d'autres forges Gitlab (ie. forge ex-IRSTEA et autres forges de centre ou d'unité) pourront ensuite être migrés progressivement par les porteurs de projet qui bénéficieront d'un appui dédié : en premier lieu l'effort du support concernera les projets tournant sur des forges de type Gitlab ;
3. Enfin les porteurs de projets hébergés sur des forges de type Redmine seront accompagnés en dernier lieu, en raison des incompatibilités techniques et organisationnelles entre les outils différents. Une étude au cas par cas devra être réalisée afin d'évaluer la pertinence de migrer et l'impact sur l'usage.

## Autres questions

N'hésitez pas à nous solliciter afin que nous mettions à jour la [FAQ]({{< ref "/page/faq" >}}).

D'ici là, soyez assurés que nous œuvrons pour faire de ce projet un succès collectif !

Par les membres du comité de pilotage de la forge INRAE.

[^1]: [ForgeMIA, la forge du département MathNum](https://forgemia.inra.fr)  
[^2]: [Plan Données pour la science d'INRAE](https://www.inrae.fr/actualites/plan-donnees-pour-la-science)   
[^3]: [démarche d'homologation SSI](https://cybersecurite.intranet.inrae.fr/je-suis-porteur-de-projet/je-suis-chef-de-projets-numeriques-ou-informatiques)
