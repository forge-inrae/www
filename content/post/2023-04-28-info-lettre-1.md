---
title: Première info-lettre
subtitle: "Info-lettre forge INRAE #1"
date: 2023-04-25T11:00:00+02:00 # à décommenter pour afficher le post si la date est passée
tags: ["info-lettre"]
---

Chères et chers collègues,

Comme vous le savez, un projet de forge logicielle institutionnelle, que nous appellerons dès maintenant la forge INRAE, est en cours d'instruction.  
Depuis février dernier, un comité de pilotage s'est formé pour mettre en place cette forge de portée institutionnelle, au bénéfice des agents et dans les lignes directrices du plan données pour la science d'INRAE[^1].

## Comité de pilotage

Ce comité de pilotage fait suite à une étude participative de pré-figuration de forge institutionnelle portée par la DipSO. Il est transverse à plusieurs départements de recherche, la DipSO[^2] et la DSI[^3]. Il s’appuie sur l’expertise d’une équipe opérationnelle constituée des administrateurs de la forge du département MathNum (forgeMIA) elle-même renforcée par des ingénieurs de la DSI.

Le comité de pilotage est composé comme suit :

- Estelle Ancelet | Christian Poirier (représentants équipe opérationnelle, administrateurs forgeMIA, MathNum)
- Michel Bamouni (DipSO)
- Nicolas Cazenave | Fanny Dedet (DSI-SolApp)
- Patrick Chabrier (RDS[^4] MathNum[^5])
- Julien Cufi (RDS TRANSFORM[^6])
- Raphaël Flores (BAP[^7], responsable comité de pilotage)
- Tovo Rabemanantsoa (DipSO, en charge du comité utilisateurs)

## Questions prégnantes

De nombreuses questions ont pu émerger suite à cette annonce : que ça soit sur l'intérêt d'une telle forge, l'impact sur les utilisateurs des forges existantes, ou les services qui seraient mis à disposition.  
Nous proposons d'y répondre en mettant en place un site web dédié[^8] au projet : <https://forge-inrae.pages.mia.inra.fr/www>.  
Il présente d'ores et déjà :

- une page rappelant la constitution du comité de pilotage ;
- une page dédiée aux questions les plus fréquentes à ce jour et à venir ;
- une série d'articles reprenant la présente info-lettre et celles à venir.

Ce site web constituera la vitrine du projet et évoluera avec des infos-lettres qui feront le point sur les avancées du projet. Nous mettrons le focus sur l'un ou l'autre des services de la forge et apporterons des informations sur sa mise en place.

Nous présentons ci-après quelques éléments de réponse à certaines interrogations légitimes.

### Stratégie de migration vers la forge INRAE

**Quelle sera la stratégie de migration vers la forge INRAE depuis forgeMIA et d'autres forges d'unités ?**

Ceci est encore à l'étude, en attente de certaines vérifications techniques. Dès que nous aurons du concret, nous vous en préciserons les modalités. L'objectif est de proposer un processus aussi stable que possible, aussi simple que nécessaire.

### Comité utilisateurs

**Comment seront considérés les besoins des utilisateurs ?**

En complément de l'expérience accumulée par les membres du comité de pilotage et de l'équipe opérationnelle, afin de prendre en compte le plus large panel de besoins, un comité utilisateur est en cours de constitution.  
Dans une double approche *top-down/bottom-up*, un appel à participation sera réalisé dans les prochaines semaines via plusieurs réseaux institutionnels, en complément de la sollicitation des départements de recherche.

### Autres questions

Nous tenons à répondre aux questions qui vous viendront, pour cela nous mettrons régulièrement à jour la [FAQ]({{< ref "/page/faq" >}}). Celle-ci est déjà alimentée avec d'autres éléments que vous pouvez fureter.

## Facteurs de réussite

**Quels sont les éléments clés que vous prenez en compte ?**

L'échéance de fin 2023 pour une première version de la forge INRAE, tel qu'annoncé au lancement du projet, nous oblige. À ce titre, plusieurs des facteurs de réussite ont été identifiés, **notamment** :

- le maintien des fonctionnalités déjà utilisées dans les forges existantes, en particulier les services proposés par la forgeMIA ;
- la fiabilité (stabilité, sécurité) des services fournis par la nouvelle forge INRAE ;
- un accompagnement à la migration vers la forge INRAE ;
- un accompagnement à l'usage des différents services proposés.

Les prochaines info-lettres permettront de clarifier les actions mise en œuvre pour répondre à chacun de ces facteurs de réussite.

D'ici là, soyez assurés que nous œuvrons pour faire de ce projet un succès collectif !

Les membres du comité de pilotage de la forge INRAE.

[^1]: [Plan Données pour la science](https://www.inrae.fr/actualites/plan-donnees-pour-la-science) d'INRAE  
[^2]: Direction pour la [Science Ouverte](https://www6.inrae.fr/dipso/)  
[^3]: Direction des [Systèmes d'Information](https://intranet.inrae.fr/systemes-information/)  
[^4]: Référent Données Stratégiques, [plus d'information](https://www6.inrae.fr/dipso/La-Direction-pour-la-science-ouverte/Bilan-2021-La-DipSO-en-action-pour-une-science-plus-ouverte/Faits-marquants-2021/Mise-en-place-des-reseaux-referents-donnees-operationnels-et-strategiques)  
[^5]: Département [Mathématiques et numérique](https://www.inrae.fr/departements/mathnum)  
[^6]: Département [Aliments, produits biosourcés et déchets](https://www.inrae.fr/departements/transform)  
[^7]: Département [Biologie et amélioration des Plantes](https://www.inrae.fr/departements/bap)  
[^8]: Site web du projet forge INRAE : <https://forge-inrae.pages.mia.inra.fr/www>
