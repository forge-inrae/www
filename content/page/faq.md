---
title: FAQ
subtitle: Foire aux Questions autour de la nouvelle forge
date: 2023-04-20T11:00:00+02:00 
comments: false
---

- [Transition](#transition)
  - [Comment sera gérée la transition vers la forge institutionnelle ?](#comment-sera-gérée-la-transition-vers-la-forge-institutionnelle-)
- [Planning](#planning)
  - [Quand pourra-t-on bénéficier des services de la nouvelle forge ?](#quand-pourra-t-on-bénéficier-des-services-de-la-nouvelle-forge-)
- [Usine logicielle](#usine-logicielle)
  - [Devra-t-on apprendre un nouveau système de gestion de code et d'outils de CI/CD ?](#devra-t-on-apprendre-un-nouveau-système-de-gestion-de-code-et-doutils-de-cicd-)
- [Github](#github)
  - [Pourquoi ne pas s'appuyer sur Github, la solution la plus visible par les développeurs ?](#pourquoi-ne-pas-sappuyer-sur-github-la-solution-la-plus-visible-par-les-développeurs-)
- [Accès de la forge](#accès-de-la-forge)
  - [La forge INRAE sera-t-elle restreinte uniquement aux agents INRAE ?](#la-forge-inrae-sera-t-elle-restreinte-uniquement-aux-agents-inrae-)
- [Impact utilisateurs forges existantes](#impact-utilisateurs-forges-existantes)
  - [Quel sera l'impact sur les utilisateurs des forges existantes ?](#quel-sera-limpact-sur-les-utilisateurs-des-forges-existantes-)
  - [Qu'est-il prévu pour la pérennité des URLs des dépôts actuels de la ForgeMIA ?](#quest-il-prévu-pour-la-pérennité-des-urls-des-dépôts-actuels-de-la-forgemia-)

<!-- Le nom des titres est volontairement court pour pouvoir les conserver facilement même en cas de restructuration de la page, les questions détaillées étant le premier item dans la foulée-->

## Transition

### Comment sera gérée la transition vers la forge institutionnelle ?

Le scénario de migration retenu se déploie en trois phases temporelles distinctes selon la forge logicielle d'origine utilisée. Pour rappel il existe dans INRAE des forges s'appuyant sur diverses solutions techniques telles que Gitlab ou Redmine. Selon le type de forge, la migration et l'accompagnement se feront à différents moments :

1. Les projets sous ForgeMIA seront migrés à l'occasion d'une bascule de forgeMIA vers la nouvelle forge INRAE. De ce fait, l'ensemble des projets de forgeMIA constituera le contenu initial de la forge INRAE ;
2. Les projets hébergés par d'autres forges Gitlab (ie. forge ex-IRSTEA et autres forges de centre ou d'unité) pourront ensuite être migrés progressivement par les porteurs de projet qui bénéficieront d'un appui dédié : en premier lieu l'effort du support concernera les projets tournant sur des forges de type Gitlab ;
3. Enfin les porteurs de projets hébergés sur des forges de type Redmine seront accompagnés en dernier lieu, en raison des incompatibilités techniques et organisationnelles entre les outils différents. Une étude au cas par cas devra être réalisée afin d'évaluer la pertinence de migrer et l'impact sur l'usage.

## Planning

### Quand pourra-t-on bénéficier des services de la nouvelle forge ?

La bascule de la forgeMIA vers forge INRAE est prévue du **10 au 12 mars 2025**. Une étape de test par des utilisateurs est en cours sur une instance de pré-production.

Une documentation à l'attention des utilisateurs sera mise à disposition en amont de la bascule afin que les responsables des projets existants puissent préparer la migration.

## Usine logicielle

### Devra-t-on apprendre un nouveau système de gestion de code et d'outils de CI/CD ?

La forge institutionnelle INRAE reposera sur les mêmes outils que forgeMIA et d'autres forges déjà utilisées dans l'institut.  
Gitlab restera le produit faisant tourner la forge INRAE :

- l'outil de gestion de code demeurera `git` ;
- les scripts de [CI/CD](https://docs.gitlab.com/ee/ci/) (intégration et déploiement continus) ne seront pas à réécrire dans un nouveau paradigme.

L'objectif est de capitaliser avec l'expérience accumulée ces dernières années par de nombreux agents INRAE. Les utilisateurs ne devraient donc pas être trop perdus avec cette solution institutionnelle.

## Github

### Pourquoi ne pas s'appuyer sur Github, la solution la plus visible par les développeurs ?

La forge Github est certes une solution dont la visibilité n'est plus à démontrer, mais elle possède l'inconvénient majeur d'être une solution privée appartenant à une entreprise des USA (Microsoft) depuis [plusieurs années](https://news.microsoft.com/2018/06/04/microsoft-to-acquire-github-for-7-5-billion/) (EN). Elle est donc soumise de ce fait à l'extra-territorialité du droit américain et à l'intervention des services gouvernementaux américains (cf. Cloud Act, FISA, Executive Order 12333).  
Qu'INRAE se dote d'une forge en propre permet ainsi d'écarter une insécurité juridique en lien avec les réglementations européennes et françaises, tout en renforçant les principes de souveraineté numérique.  
Par ailleurs, les modalités de collaboration avec les usagers de Github ne sont pas bloquées avec une forge institutionnelle INRAE et demeurent possibles pour les projets qui y disposent d'une partie de leur communauté.

<!-- ## Licence Gitlab Enterprise -->

<!-- ### Pourquoi ne pas avoir opté pour une licence *Enterprise* de Gitlab apportant des fonctionnalités supplémentaires ? -->

<!-- Le modèle économique [*open-core*](https://about.gitlab.com/company/pricing/#single-plan-for-one-customer) de Gitlab ne fait pas de distinction entre les types d'utilisateurs enregistrés sur l'instance, chacun d'entre eux comptant pour une licence *Enterprise*. La forge INRAE ciblant potentiellement tous les agents de l'institut et leurs partenaires directs, les offres payantes de Gitlab ne sont pas financièrement soutenables.  -->
<!-- Des solutions externes permettent de combler partiellement les fonctionnalités payantes de Gitlab. Nous étudierons à quel point il est possible d'en simplifier l'intégration ou l'usage. -->
<!-- Enfin, Gitlab a une politique d'ouverture qui fait qu'une fois une fonctionnalité *Entreprise* financée, elle peut être reversée dans la version communautaire [Pricing strategy](https://about.gitlab.com/company/pricing/#pricing-strategy)(EN). -->

## Accès de la forge

### La forge INRAE sera-t-elle restreinte uniquement aux agents INRAE ?

Le niveau d'accès de la forge INRAE reste à identifier clairement, mais les principes suivants sont d'ores et déjà actés :

- la forge institutionnelle sera ouverte sur Internet sans restriction, les projets publics y seront donc visibles automatiquement. À l'inverse les projets internes ne seront visibles que des utilisateurs disposant d'un compte. Enfin, les projets privés ne seront accessibles qu'aux membres décidés par le propriétaire du projet, selon les [paramètres de visibilité](https://docs.gitlab.com/ee/user/public_access.html) (EN) spécifiques à chaque projet ;
- l'authentification sur la forge s'appuiera sur la fédération d'identité RENATER et permettra ainsi à des partenaires de l'Enseignement Supérieur et Recherche de rejoindre un groupe pré-existant. Les compte CRU ne seront pas opérationnels pour des raisons de sécurité. Mais il sera possible de créer des comptes pour les partenaires hors fédération RENATER, en utilisant les mécanismes de création de compte externe fournis par la DSI ([IAM](https://iam.intranet.inrae.fr/)).

## Impact utilisateurs forges existantes

### Quel sera l'impact sur les utilisateurs des forges existantes ?

D'une manière générale, il est prévu que tous les services fournis par forgeMIA soient conservés dans l'instance institutionnelle. Nous prévoyons donc qu'il n'y ait aucune régression fonctionnelle.

Nous réfléchissons à réduire autant que possible l'impact sur les utilisateurs actuels, en particulier ceux de forgeMIA. Un test de migration est prévu dans le courant de l'été 2024 pour évaluer le bon fonctionnement des outils de migration côté administrateur et pour affiner l'impact sur les utilisateurs finaux.

### Qu'est-il prévu pour la pérennité des URLs des dépôts actuels de la ForgeMIA ?

Pour forgeMIA nous avons prévu un mécanisme de redirection web (alias DNS ou redirection web) pour ne pas casser les liens web vers les projets existants. Notamment ceux renseignés dans les publications scientifiques.
Pour les autres forges, l'opération est plus délicate dès lors que nous n'avons pas la main sur les domaines et qu'il reste des projets dans la forge source. Il faudrait créer une liste des espaces de noms pour lesquels il faudrait  rediriger.