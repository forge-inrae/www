---
title: Formations
subtitle: Accompagnements utilisateurs
date: 2024-12-12T11:00:00+02:00 
comments: false
tags: ["accompagnement"]
---

En marge du projet forge logicielle INRAE, plusieurs initiatives d'accompagnement ont émergé, certaines de manière indépendantes, d'autres avec le support du projet forge logicielle institutionnelle.  
Cette page en référence certaines pour simplifier leur identification par les agents INRAE qui en constituent le public cible.
<!-- Le nom des titres est volontairement court pour pouvoir les conserver facilement même en cas de restructuration de la page, les questions détaillées étant le premier item dans la foulée-->

## Formations locales par centre

Un groupe d’agents du site PACA d’INRAE s’est constitué fin 2022 pour concevoir une formation sur Git et GitLab destinée aux agents de leurs unités. Cette formation, organisée en trois sessions, s’adresse principalement aux collègues non-informaticiens de l’institut et a déjà été dispensée avec succès dans plusieurs unités. Elle est particulièrement appréciée pour son accessibilité, étant conçue pour les utilisateurs de rStudio et VScode qui ne sont pas familiers avec la ligne de commande, ni avec des concepts trop avancés d’informatique. Les supports de formation sont accessibles à l’adresse [Git GitLab PACA / Support de formation](https://forgemia.inra.fr/git-gitlab-paca/support-de-formation).

Face à la demande de collègues d’autres sites et pour profiter de l’arrivée de la future forge institutionnelle, nous avons constitué une communauté de formateurs pour essaimer cette formation au-delà de la région PACA. Nous sommes à la recherche de nouveaux formateurs sur les centres non pourvus en particulier, ayant une volonté de partager leurs connaissances de Git et GitLab avec des collègues. Il n’est pas nécessaire d’en avoir une utilisation très avancée pour donner cette formation.

Nous avons déjà identifié plus d'une trentaine de volontaires avec l’aide de l’équipe FTLV dans les centres suivants :

- Antilles-Guyane
- Bourgogne-Franche-Comté
- Bretagne-Normandie
- Clermont-Auvergne-Rhônes-Alpes
- Corse
- Hauts-de-France
- Île-de-France - Jouy-en-Josas - Antony
- Île-de-France - Versailles-Saclay
- Lyon-Grenoble-Auvergne-Rhône-Alpes
- Nouvelle-Aquitaine Bordeaux
- Occitanie-Montpellier
- Occitanie-Toulouse
- Pays de la Loire
- Provence-Alpes-Côte d’Azur
- Val de Loire

Nous recherchons toujours des représentants dans plusieurs centres INRAE, notamment les centres non pourvus :

- Centre siège
- Grand Est - Colmar
- Grand Est - Nancy
- Nouvelle-Aquitaine Poitiers

Si vous souhaitez contribuer à la diffusion de ces outils autour de vous, nous vous invitons à vous joindre à notre communauté de formateurs. Ainsi, vous joueriez un rôle clé dans la diffusion de connaissances et de bonnes pratiques en gestion de code et vous ferez partie d’une communauté qui pourra vous venir en aide si vous en avez besoin lors d’une formation.

Pour nous contacter : [Jean-Baptiste Louvet et Raphaël Flores](mailto:jean-baptiste.louvet@inrae.fr,raphael.flores@inrae.fr?subject=Formation%20de%20formateurs%20INRAE%20sur%20Git%20%26%20GitLab).

## Formation Git et GitLab CSIESR

La [DSI a annoncé](https://forum.dipso.inrae.fr/t/adhesion-au-catalogue-formations-informatiques-csiesr/2815) en début d'année 2024 l'adhésion à un catalogue de formations en partie subventionnées par le Ministère de l'Enseignement Supérieur et de la Recherche (MESR). Le catalogue de formation est tenu à jour sur ce site : <https://formac.csiesr.eu/offreDomaine/3/>.
Plusieurs formations Git et GitLab y sont proposées à intervalles réguliers :

- [Git : Gestion des dépôts](https://formac.csiesr.eu/formation/f3b2bdbc-a00c-423e-b0f2-26520e5f9173), sur deux jours, dont l'objectif est de :
  - Mettre en place une solution de configuration logicielle basée sur Git ;
  - Gérer les versions des projets du dépôt de données.
- [Git : Gestion de dépôts + Gitlab-CI / Intégration continue](https://formac.csiesr.eu/formation/86181f58-6d35-4b46-85c2-7f275f5da048), sur cinq jours. Les objectifs sont de :
  - Maîtriser l'usage de commandes Git pour la gestion d'un dépôt de sources
  - Mettre en œuvre et exploiter un serveur d'intégration continue
  - Gérer les interconnexions avec un système de build et de tests

## Modules de formation Oscar - GPACL

La Direction pour la Science Ouverte d'INRAE (DipSO) vous propose un [parcours de formations en e-learning](https://elearning.formation-permanente.inrae.fr/course/view.php?id=668) évolutif, riche, pratique, dédié à l'ouverture de la science.  
Conçue pour accompagner tous les agents de l'institut dans l'évolution des pratiques de recherche, cette formation vous apportera les connaissances et les outils suffisants pour vous lancer dans le mouvement pour la science ouverte ou approfondir vos connaissances.
Oscar peut être suivi de façon linéaire ou être consulté à la carte en fonction de vos besoins.
Aujourd'hui, trois modules de formation sont disponibles :

- [Les fondamentaux de la science ouverte](https://elearning.formation-permanente.inrae.fr/course/view.php?id=682&section=0)
- [Gestion et partage des données](https://elearning.formation-permanente.inrae.fr/course/view.php?id=672&section=0)
- [Publications ouvertes](https://elearning.formation-permanente.inrae.fr/course/view.php?id=673&section=0)
- [Sciences et recherches participatives](https://elearning.formation-permanente.inrae.fr/course/view.php?id=675)

Les autres sont en cours de conception. Vous serez informés de leur sortie. Mais l'un d'entre eux concernera directement la Gestion et Partage des Algorithmes, Codes et Logiciels (GPACL).
