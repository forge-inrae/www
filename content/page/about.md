---
title: Qui sommes-nous ?
subtitle: Les collectifs du projet forge institutionnelle
comments: false
---

Plusieurs collectifs étroitement liés travaillent au succès du projet forge logicielle institutionnelle.

## Comité de pilotage

Le comité de pilotage a été créé en février 2023.

Il regroupe des représentants de plusieurs départements de recherche, de la DipSO[^0], de la DSI[^1] et s'appuie sur l'expertise d'une équipe opérationnelle constituée des administrateurs de la forge du département MathNum ([forgeMIA](https://forgemia.inra.fr/)) renforcée par des ingénieurs de la DSI.

Le comité de pilotage est composé comme suit :

- Estelle Ancelet | Christian Poirier (administrateurs forgeMIA/forge INRAE, MathNum[^3])
- Michel Bamouni (DipSO)
- Nicolas Cazenave | Fanny Dedet (DSI-SolApp[^9])
- Patrick Chabrier (RDS[^2] MathNum)
- Julien Cufi (RDS TRANSFORM[^4])
- Raphaël Flores (BAP[^5], responsable comité de pilotage)
- Eddie Iannuccelli (DSI-Infra[^8])
- Tovo Rabemanantsoa (DipSO, en charge du comité utilisateurs)
- Martin Souchal (DipSO)

## Équipe opérationnelle

L'équipe opérationnelle est composée, dans la phase projet, d'agents du département scientifique MathNum, ainsi que d'agents de la DSI et de la DipSO. Sa mission est de développer et de déployer la forge de portée institutionnelle, sur une infrastructure à la hauteur des attentes des agents et dans le respect des lignes directrices du Plan Données pour la Science d'INRAE[^6].

Les membres de l'équipe opérationnelle disposent d'expertises complémentaires, apportées par des administrateurs de la forge MIA[^7], de la forge Gitlab ex-IRSTEA, ainsi que par des administrateurs système et réseau de la DSI et de la DipSO.

L'équipe opérationnelle est composée comme suit :

- Estelle Ancelet (MathNum)
- Michel Candido (DipSO)
- Sylvain Lanoë (MathNum)
- Guillaume Perréal (DSI-SolApp)
- Christian Poirier (MathNum)

Les experts en appui sont:

- Aurélien Perillat-Bottonet (DSI-Infra)
- Matthieu Simon (DSI-Infra)
- Anthony Thiriot (DSI-Infra)
- Hervé Toureille (DSI-Infra)

La DSI s'est également engagée en apportant son expertise au niveau le la sécurité des systèmes et fournit les ressources numériques (machines virtuelles, réseau, espaces de stockages) nécessaires au fonctionnement du service dans la durée.

Enfin, une prestation de service a été contractualisée pour accompagner l'équipe opérationnelle dans le développement d'outils dédié à l'exploitation de Gitlab et à leur prise en main.

## Comité d'utilisateurs

Le Comité des Utilisateurs d'une trentaine de personnes représente les personnels de l'Institut. Il a pour vocation de structurer la remontée des besoins de la communauté en matière d'usages de la forge INRAE afin qu'ils soient pris en compte de façon satisfaisante.

[^0]: Direction pour la [Science Ouverte](https://science-ouverte.inrae.fr/fr/la-science-ouverte/la-direction-pour-la-science-ouverte)  
[^1]: Direction des [Systèmes d'Information (DSI)](https://systemes-information.intranet.inrae.fr/organisation-de-la-dsi)  
[^2]: Référent Données Stratégiques, [plus d'information](https://science-ouverte.inrae.fr/fr/les-donnees-scientifiques/les-reseaux-metiers/les-referents-donnees-strategiques)  
[^3]: Département [Mathématiques et numérique](https://www.inrae.fr/departements/mathnum)  
[^4]: Département [Aliments, produits biosourcés et déchets](https://www.inrae.fr/departements/transform)  
[^5]: Département [Biologie et amélioration des Plantes](https://www.inrae.fr/departements/bap)  
[^6]: [Plan Données pour la science d'INRAE](https://www.inrae.fr/actualites/plan-donnees-pour-la-science)  
[^7]: [ForgeMIA, la forge du département MathNum](https://forgemia.inra.fr)  
[^8]: DSI - [Département Infrastructures](https://systemes-information.intranet.inrae.fr/organisation-de-la-dsi/departement-infrastructures)  
[^9]: DSI - [Département Solutions Applicatives](https://systemes-information.intranet.inrae.fr/organisation-de-la-dsi/departement-solutions-applicatives)  
