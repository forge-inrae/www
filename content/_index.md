# Projet forge institutionnelle INRAE

Les codes et logiciels sont considérés comme des produits de la recherche à part entière dont l’ouverture, le partage et la valorisation constituent des enjeux forts pour une science ouverte, reproductible et cumulative.

Pour renforcer son accompagnement de ces enjeux stratégiques, INRAE se dote d’une forge institutionnelle visant à fournir aux équipes de recherche qui le souhaitent des outils et un accompagnement pour leurs productions informatiques. Il s’agit d’une part de faciliter le développement collaboratif de codes et logiciels dans le respect des standards de qualité en matière de développement logiciel, et, d’autre part, de proposer des possibilités simples et sécurisées de tests des codes et logiciels sur différents environnements (développement, test, pré-production et production).

Sous la gouvernance du bureau du plan données pour la science, un comité de pilotage de cette forge a été mis en place. A compter du 1er février 2023, celui-ci va conduire le chantier et prendra en considération l’existant en terme d’expertise et de services mais aussi l’accompagnement nécessaire des nouveaux utilisateurs. Pour les projets hébergés dans des forges existantes, il veillera à en faciliter pour les utilisateurs qui le souhaitent la transition vers la forge institutionnelle lorsque celle-ci sera en mesure de les accueillir.

Dans l’attente de la mise en production de la forge institutionnelle dont la date est prévue le **12 mars 2025**, le bureau du plan données pour la science encourage l’utilisation de la forge MIA (<https://forgemia.inra.fr/>) pour le démarrage de nouveaux projets.  
Pour les projets déjà en cours dans des forges existantes, un accompagnement à la migration vers la forge institutionnelle sera proposé en amont et en aval de la [mise en production](./page/faq/#planning).

Contact : [copil-forge-inrae@inrae.fr](mailto:copil-forge-inrae@inrae.fr?subject=[Question%20site%20web]%20)
